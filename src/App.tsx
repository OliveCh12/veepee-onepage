import React from 'react';

// Components
import Header from "./components/Header"
import Footer from "./components/Footer"

import Select from './components/Select'
import List from "./components/List"
import Info from "./components/Info"
import Subscription from './components/Subscription'



const App: React.FC = () => {
  return (
    <div className="App">
      <Header />
      <main className="container">
        <Select position="center" />
        <List limit={6} />
        <Info />
        <Subscription />
      <Footer />

      </main>
    </div>
  );
}

export default App;
