import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight, faStar } from '@fortawesome/free-solid-svg-icons';

interface Props {
    country: string;
    place: string;
    label: string;
    tags: any;
    upto: string;
    rating: number;
}

const Item: React.FC<Props> = props => {
    const stars: any = []

    // Stars Components
    const Stars = () => {
        for (let index = 0; index < props.rating; index++) {
            stars.push(<FontAwesomeIcon className="card-rating" icon={faStar} />)
        }
    }

    Stars()

    // Badges Components
    const Badges = () => {
        return props.tags.slice(0, 2).map((tag: any, index: number) => (
            <span key={index} className={`card-badge ${tag.classname}`}>
                {tag.label}
            </span>
        ));
    }

    return (
        <div className="card">
            <div className="card-thumbnail-container">
                <span className="card-thumbnail-info">{props.upto}</span>
                <img
                    className="card-thumbnail"
                    src="https://images.unsplash.com/photo-1468532988488-ffa1203ee384?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=10"
                />
            </div>
            <div className="card-body">
                <h5 className="card-title">
                    {props.country} - <span>{props.place}</span>
                </h5>
                <p className="card-info">{props.label} - {stars}</p>
                <div className="card-badges">
                    <Badges />
                </div>
                <a className="card-link" href="/">
                    <FontAwesomeIcon icon={faArrowRight} />
                </a>
            </div>
        </div>
    );
};

export default Item;

