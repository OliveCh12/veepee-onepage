import React from 'react'

const Info: React.FC = () => {
    return (
        <section id="section-info">
            <p>Comment bénéficier de l'offre ?</p>
            <p><strong>150 € de réduction* dès 1000€ d'achat</strong></p>
        </section>
    )
}

export default Info
