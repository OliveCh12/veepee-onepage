import React from 'react'

const Header: React.FC = () => {
    return (
        <header>
            <h1>Découvrez les offres du moment</h1>
            <p>Vol + hotel jusqu'à -70%</p>
        </header>
    )
}

export default Header
