import React, { useState } from 'react'
// Components imports
import Item from "./Item"
// Data imports
import destinations from "../destinations.json"

interface Props {
    limit: number
}


const List: React.FC<Props> = props => {
    const [state] = useState(destinations)

    return (
        <section id="section-destinations">
            {state.slice(0, props.limit).map((item, index) => (
                <Item key={index} country={item.country} place={item.place} label={item.label} tags={item.tags} upto={item.upto} rating={item.rating}/>
            ))}
        </section>
    )
}

export default List

