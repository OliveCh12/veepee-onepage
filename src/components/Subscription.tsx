import React from 'react'

interface Props {

}

const Subscription: React.FC<Props> = () => {
    return (
        <section id="section-subscription">
            <div className="subscription-header">
                <span><strong>Déjà Membre ?</strong></span>
                <p>Votre code promo vous attend directement sur le site <a href="/">en cliquant ici</a></p>
            </div>
            <div className="subscription-body">
                <div className="step">
                    <div className="step-number">1</div>
                    <div className="step-content">
                        <span>Rejoingez</span>
                        <span><strong>Emirates | The List</strong></span>
                    </div>
                </div>
                <div className="step">
                    <div className="step-number">2</div>
                    <div className="step-content">
                        <span>Recevez par Email</span>
                        <span><strong>Votre bon de 150 € offert</strong></span>
                        <span>à utiliser dès 1000 € d'achat</span>
                    </div>
                </div>
                <div className="step">
                    <div className="step-number">3</div>
                    <div className="step-content">
                        <span>Réservez votre séjour</span>
                        <span><strong>Avant le 30 avril 2019</strong></span>
                    </div>
                </div>
            </div>
            <div className="subscription-footer">
                <button className="btn">Je m'inscris</button>
            </div>

        </section >
    )
}

export default Subscription
