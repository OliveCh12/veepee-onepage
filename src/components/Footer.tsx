import React from 'react'

const Footer: React.FC = () => {
    return (
        <footer>
            <span>Rejoignez <strong>Emirates | The List</strong></span>
            <span>Vol + hôtel négociés jusqu'à -70%</span>
        </footer>
    )
}

export default Footer
