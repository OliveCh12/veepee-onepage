import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSlidersH } from '@fortawesome/free-solid-svg-icons'

// Data
import destinations from "../destinations.json"

interface Props {
    position: string
}

const Select: React.FC<Props> = props => {

    const [state, setState] = useState("Default")

    const position = {
        display: "flex",
        justifyContent: props.position,
        margin: "2rem 0rem",
    }

    return (
        <div className="form-group" style={position}>
            <select className="form-input" value={state} onChange={(event => { setState(event.target.value) })}>
                <option disabled>Destination</option>
                {destinations.map((destination, index) => (
                    <option key={index} value={destination.place}>{destination.place}</option>
                ))}
            </select>
        </div>
    )
}

export default Select
